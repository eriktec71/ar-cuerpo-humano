package com.tec.ar_cuerpo_humano.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.tec.ar_cuerpo_humano.R
import com.tec.ar_cuerpo_humano.databinding.FragmentComponentBinding
import com.tec.ar_cuerpo_humano.repository.State.*
import com.tec.ar_cuerpo_humano.repository.data.BodyPart
import com.tec.ar_cuerpo_humano.ui.common.BaseFragment
import com.tec.ar_cuerpo_humano.ui.common.ComponentAdapter
import com.tec.ar_cuerpo_humano.util.toast
import com.tec.ar_cuerpo_humano.vm.ComponentViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ComponentFragment : BaseFragment<FragmentComponentBinding, ComponentViewModel>() {

    override fun getFragmentId() = R.layout.fragment_component

    override fun getViewModel() = ComponentViewModel::class.java

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        lifecycleScope.launch {
            viewModel.uiState.collect {
                when (it) {
                    is Loading -> { toast("Cargando Datos") }
                    is Success -> { setUpRecycler(it.data) }
                    is Error -> { toast(it.error.message.toString()) }
                }
            }
        }
    }

    private fun setUpRecycler(list: List<BodyPart>) {
        binding.componentRecyclerView.apply {
            adapter = ComponentAdapter(list) { bodyPart ->  startAr(bodyPart) }
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun startAr(bodyPart: BodyPart) {
        val sceneViewIntent = Intent(Intent.ACTION_VIEW)
        val intent = Uri.parse("https://arvr.google.com/scene-viewer/1.0").buildUpon()
            .appendQueryParameter("file", bodyPart.model3d)
            .appendQueryParameter("title", bodyPart.name)
            .build()
        sceneViewIntent.data = intent
        sceneViewIntent.setPackage("com.google.ar.core")
        startActivity(sceneViewIntent)
    }
}