package com.tec.ar_cuerpo_humano.ui.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tec.ar_cuerpo_humano.model.server.ServerDataSource
import com.tec.ar_cuerpo_humano.repository.Repository
import com.tec.ar_cuerpo_humano.vm.MyViewFactory

abstract class BaseFragment<VB : ViewDataBinding, VM : ViewModel> : Fragment() {

    protected lateinit var binding: VB
    protected val viewModel: VM by lazy {
        ViewModelProvider(
            this,
            MyViewFactory(Repository(ServerDataSource()))
        )[getViewModel()]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getFragmentId(), container, false)
        return binding.root
    }

    protected abstract fun getFragmentId(): Int

    protected abstract fun getViewModel(): Class<VM>
}