package com.tec.ar_cuerpo_humano.ui

import android.os.Bundle
import android.view.View
import androidx.navigation.Navigation
import com.tec.ar_cuerpo_humano.R
import com.tec.ar_cuerpo_humano.databinding.FragmentLoginBinding
import com.tec.ar_cuerpo_humano.ui.common.BaseFragment
import com.tec.ar_cuerpo_humano.util.NavigateHost
import com.tec.ar_cuerpo_humano.vm.LoginViewModel

class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>(), NavigateHost {

    override fun getFragmentId() = R.layout.fragment_login

    override fun getViewModel() = LoginViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.navHost = this
        binding.user = viewModel.user
        binding.lifecycleOwner = viewLifecycleOwner
    }

    override fun navigateTo(view: View) {
        Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_homeFragment)
    }
}