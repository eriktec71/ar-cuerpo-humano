package com.tec.ar_cuerpo_humano.ui

import android.os.Bundle
import android.view.View
import androidx.navigation.Navigation
import com.tec.ar_cuerpo_humano.R
import com.tec.ar_cuerpo_humano.databinding.FragmentHomeBinding
import com.tec.ar_cuerpo_humano.ui.common.BaseFragment
import com.tec.ar_cuerpo_humano.util.NavigateHost
import com.tec.ar_cuerpo_humano.util.toast
import com.tec.ar_cuerpo_humano.vm.HomeViewModel

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(), NavigateHost {

    override fun getFragmentId() = R.layout.fragment_home

    override fun getViewModel() = HomeViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.navigateHost = this
        binding.lifecycleOwner = viewLifecycleOwner
    }

    override fun navigateTo(view: View) {
        val navController = Navigation.findNavController(view)
        when (view.id) {
            R.id.add_user_card_view -> { requireContext().toast("Add User") }
            R.id.delete_user_card_view -> { requireContext().toast("Delete User") }
            R.id.edit_user_card_view -> { requireContext().toast("Edit User") }
            R.id.consult_info_card_view -> { navController.navigate(R.id.action_homeFragment_to_bodyPartsFragment) }
        }
    }
}