package com.tec.ar_cuerpo_humano.ui

import android.os.Bundle
import android.view.View
import androidx.navigation.Navigation
import com.tec.ar_cuerpo_humano.R
import com.tec.ar_cuerpo_humano.databinding.FragmentBodyPartsBinding
import com.tec.ar_cuerpo_humano.ui.common.BaseFragment
import com.tec.ar_cuerpo_humano.util.NavigateHost
import com.tec.ar_cuerpo_humano.util.toast
import com.tec.ar_cuerpo_humano.vm.BodyPartsViewModel

class BodyPartsFragment : BaseFragment<FragmentBodyPartsBinding, BodyPartsViewModel>(), NavigateHost {

    override fun getFragmentId() = R.layout.fragment_body_parts

    override fun getViewModel() = BodyPartsViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.navigate = this
        binding.lifecycleOwner = viewLifecycleOwner
    }

    override fun navigateTo(view: View) {
        when(view.id) {
            R.id.head_card_view -> Navigation.findNavController(view).navigate(R.id.action_bodyPartsFragment_to_componentFragment)
            else -> { requireContext().toast("Hola") }
        }
    }
}