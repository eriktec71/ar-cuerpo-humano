package com.tec.ar_cuerpo_humano.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tec.ar_cuerpo_humano.R
import com.tec.ar_cuerpo_humano.databinding.ActivityMainBinding
import com.tec.ar_cuerpo_humano.util.contentView

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by contentView(R.layout.activity_main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding
    }
}