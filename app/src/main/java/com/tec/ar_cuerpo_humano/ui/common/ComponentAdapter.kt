package com.tec.ar_cuerpo_humano.ui.common

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.tec.ar_cuerpo_humano.R
import com.tec.ar_cuerpo_humano.databinding.CardComponentBinding
import com.tec.ar_cuerpo_humano.repository.data.BodyPart

class ComponentAdapter(
    private val listComponents: List<BodyPart>,
    private val callback : (BodyPart) -> Unit
) : RecyclerView.Adapter<ComponentAdapter.ComponentViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentViewHolder {
        val binding =
            CardComponentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ComponentViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ComponentViewHolder, position: Int) = with(holder) {
        holder.itemView.animation = AnimationUtils.loadAnimation(holder.itemView.context, R.anim.fade_scale_anim)
        insertInfo(listComponents[position])
    }

    override fun getItemCount() = listComponents.size

    inner class ComponentViewHolder(private val binding: CardComponentBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun insertInfo(bodyPart: BodyPart) {
            binding.titleCard.text = bodyPart.name
            binding.bodyCard.text = bodyPart.about
            binding.seeButton.setOnClickListener { callback(listComponents[adapterPosition]) }
        }
    }
}