package com.tec.ar_cuerpo_humano.util

import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import kotlin.reflect.KProperty

class ContentViewBindingDelegate<in R : AppCompatActivity, out VB : ViewDataBinding>(
    @LayoutRes private val layoutRes: Int
) {
    private var binding: VB? = null

    operator fun getValue(activity: R, kProperty: KProperty<*>): VB {
        if (binding == null) {
            binding = DataBindingUtil.setContentView<VB>(activity, layoutRes)
                .apply { lifecycleOwner = activity }
        }
        return binding!!
    }
}

fun <R : AppCompatActivity, VB : ViewDataBinding> contentView(
    @LayoutRes layoutRes: Int
): ContentViewBindingDelegate<R, VB> = ContentViewBindingDelegate(layoutRes)