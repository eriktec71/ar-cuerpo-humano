package com.tec.ar_cuerpo_humano.util

import android.view.View

interface NavigateHost {

    fun navigateTo(view: View)
}