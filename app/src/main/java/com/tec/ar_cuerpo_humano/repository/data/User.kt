package com.tec.ar_cuerpo_humano.repository.data

data class User(
    var email: String = "",
    var password: String = ""
)
