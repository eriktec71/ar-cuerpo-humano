package com.tec.ar_cuerpo_humano.repository.data

data class BodyPart(
    var name: String = "",
    var img: String = "",
    var model3d: String = "",
    var about: String = ""
)