package com.tec.ar_cuerpo_humano.repository

import com.tec.ar_cuerpo_humano.model.util.toBodyPart
import com.tec.ar_cuerpo_humano.repository.data.BodyPart
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class Repository(private val serverRepository: ServerRepository) {

    fun getAll(): Flow<State<List<BodyPart>>> = flow {
        serverRepository.getAll()
            .catch { exception -> State.Error(Exception(exception.message)) }
            .collect { listRepo -> emit(State.Success(listRepo.map { bodyPart -> bodyPart.toBodyPart() })) }
    }
}