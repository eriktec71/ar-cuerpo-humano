package com.tec.ar_cuerpo_humano.repository

sealed class State<out T> {
    object Loading: State<Nothing>()
    data class Success<out T>(val data: T): State<T>()
    data class Error(val error: Exception): State<Nothing>()
}
