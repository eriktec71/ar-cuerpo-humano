package com.tec.ar_cuerpo_humano.repository

import com.tec.ar_cuerpo_humano.model.data.BodyPart
import kotlinx.coroutines.flow.Flow

interface ServerRepository {

    fun getAll(): Flow<List<BodyPart>>
}