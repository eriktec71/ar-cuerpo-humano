package com.tec.ar_cuerpo_humano.model.data

data class BodyPart(private val map: Map<String, Any>) {
    val name: String by map
    val img: String by map
    val model3d: String by map
    val about: String by map
}