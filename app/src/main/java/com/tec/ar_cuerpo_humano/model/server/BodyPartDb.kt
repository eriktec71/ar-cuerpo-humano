package com.tec.ar_cuerpo_humano.model.server

import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await

class BodyPartDb {

    private val db = Firebase.firestore

    suspend fun getAll(): QuerySnapshot {
        return db.collection("Organs").get().await()
    }
}