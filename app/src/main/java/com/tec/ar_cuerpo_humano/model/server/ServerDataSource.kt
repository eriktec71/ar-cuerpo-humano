package com.tec.ar_cuerpo_humano.model.server

import com.tec.ar_cuerpo_humano.repository.ServerRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import com.tec.ar_cuerpo_humano.model.data.BodyPart as BodyPartServer

class ServerDataSource : ServerRepository {

    private val bodyPartDb = BodyPartDb()

    @ExperimentalCoroutinesApi
    override fun getAll(): Flow<List<BodyPartServer>> = callbackFlow {
        val querySnapshot = bodyPartDb.getAll()
        val listBody: List<BodyPartServer> = querySnapshot.map { queryDocumentSnapshot ->
            BodyPartServer(queryDocumentSnapshot.data)
        }
        trySend(listBody).isSuccess
        awaitClose { close() }
    }
}
