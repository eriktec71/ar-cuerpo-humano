package com.tec.ar_cuerpo_humano.model.util

import com.tec.ar_cuerpo_humano.model.data.BodyPart as BodyPartServer
import com.tec.ar_cuerpo_humano.repository.data.BodyPart

fun BodyPartServer.toBodyPart(): BodyPart = BodyPart(
    name,
    img,
    model3d,
    about
)