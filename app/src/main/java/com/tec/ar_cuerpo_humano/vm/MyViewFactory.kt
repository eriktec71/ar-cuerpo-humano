package com.tec.ar_cuerpo_humano.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tec.ar_cuerpo_humano.repository.Repository

class MyViewFactory(private var repository: Repository) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(BodyPartsViewModel::class.java) -> BodyPartsViewModel() as T
            modelClass.isAssignableFrom(HomeViewModel::class.java) -> HomeViewModel() as T
            modelClass.isAssignableFrom(LoginViewModel::class.java) -> LoginViewModel() as T
            modelClass.isAssignableFrom(ComponentViewModel::class.java) -> ComponentViewModel(repository) as T
            else -> throw IllegalArgumentException("View Model No Fount")
        }
    }
}