package com.tec.ar_cuerpo_humano.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tec.ar_cuerpo_humano.repository.Repository
import com.tec.ar_cuerpo_humano.repository.State
import com.tec.ar_cuerpo_humano.repository.data.BodyPart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class ComponentViewModel(private val repository: Repository) : ViewModel() {

    private val _uiState = MutableStateFlow<State<List<BodyPart>>>(State.Loading)
    val uiState: StateFlow<State<List<BodyPart>>>
    get() = _uiState

    init {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getAll().collect { result -> _uiState.value = result }
        }
    }
}